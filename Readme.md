#Инструкция по запуску

#####Нужно добавить переменную и указать папку где лежит geckodriver
#####Скачиваем отсюда
[GeckoDriver](https://github.com/mozilla/geckodriver/releases)

`"webdriver.gecko.driver" "c:/../../geckodriver.exe"`

#####Далее
`git clone `

`cd test_task`

`mvn clean test`

`mvn allure:serve`
